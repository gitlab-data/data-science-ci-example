import yaml

class ScoringParameters:
    def __init__(self, joblib_file, fields, decile_cuts=[]):
        self.data = {
            "model": {
                "artifacts": {"joblib_file": joblib_file},
                "fields": fields,
                "decile_cuts": decile_cuts,
            }
        }

    def to_yaml(self, file_name):
        if not file_name.endswith(".yaml"):
            raise Exception("file extension error, use .yaml")

        else:
            with open(file_name, "w") as f:
                yaml.safe_dump(self.data, f, sort_keys=False)

    def __repr__(self):
        return f"{self.data}"

    def __str__(self):
        return str(self.data)
    
    
