FROM python:3.10-slim
COPY .    /app/
WORKDIR /app
RUN apt-get update
RUN apt-get install -y git

# Install uv for faster install of python packages
RUN apt-get -y install curl && curl -LsSf https://astral.sh/uv/install.sh | sh

# Install Python Packages via uv
RUN /root/.local/bin/uv pip install --system --no-cache -r requirements.txt

